// Ex1: Tìm Số Nguyên Dương Nhỏ Nhất
function findMin() {
    for (var num1 = 0, num2 = 0, count = 1; count < 1e4; count++)
        if (num1 += count,
        console.log(num1),
        num1 > 1e4) {
            console.log(count),
            num2 = count;
            break
        }
    document.getElementById("txtResult1").innerHTML = "Số nguyên dương nhỏ nhất: " + num2
}
// Ex2: Tính Tổng
function tinhTong() {
    for (var num1 = document.getElementById("inputX").value, num2 = document.getElementById("inputN").value, count = 0, Tong = 1; Tong <= num2; Tong++)
        count += Math.pow(num1, Tong);
    document.getElementById("txtResult2").innerHTML = "Tổng: " + count
}
// Ex3 : Tính Giai Thừa
function tinhGT() {
    for (var num1 = document.getElementById("inputN1").value , num2 = 1, count = 1; count <= num1; count++)
        num2 *= count;
    document.getElementById("txtResult3").innerHTML = "Giai thừa: " + num2
}
// Ex4 : Tạo Div
function taoDiv() {
    for (var num1 = "", num2 = 1; num2 <= 10; num2++)
        num1 += num2 % 2 == 0 ? "<div class='bg-danger text-white p-2'>Div chẵn </div>" : "<div class='bg-primary text-white  p-2'>Div lẻ</div>";

    document.getElementById("txtResult4").innerHTML = num1
}

